public class SequentialRadixSorter implements Sorter {

    int useBits;

    SequentialRadixSorter(int useBits) {
        this.useBits = useBits;
    }

    @Override
    public int[] sort(int[] unsortedArray) {

        int[] a = unsortedArray;

        // STEP A - Find max value
        int max = 0;
        for(int i=0; i < a.length; i++)
            if(a[i] > max) max = a[i];

        // Discover how many bits the max value needs
        int numBits = 1;
        while(max >= (1L << numBits)) numBits++;

        // Calculate how many digits we need
        int numDigits = Math.max(1, numBits/useBits);

        int[] bit = new int[numDigits];

        int rest = numBits % numDigits;

        // Distribute the bits over the digits
        for(int i=0; i < bit.length; i++) {
            bit[i] = numBits/numDigits;

            if(rest-- > 0) bit[i]++;
        }

        int[] b = new int[a.length];

        int shift = 0;

        for(int i=0; i < bit.length; i++) {
            radixSort(a, b, bit[i], shift);
            shift += bit[i];

            int[] tmp = a;
            a = b;
            b = tmp;
        }

        return a;
    }


    private void radixSort(int[] a, int[] b, int maskLen, int shift) {

        // The size / mask of the digit we are interested in this turn
        int mask = (1<< maskLen) - 1;

        // The count of each digit
        int[] digitFrequency = new int[mask+1];

        // STEP B - Count frequency of each digit
        for(int i=0; i < a.length; i++) {
            digitFrequency[(a[i] >>> shift) & mask]++;
        }

        // STEP C - Calculate pointers for digits
        int accumulated = 0;
        int[] digitPointers = new int[digitFrequency.length];

        for(int i=0; i < digitFrequency.length; i++) {
            digitPointers[i] = accumulated;
            accumulated += digitFrequency[i];
        }

        // STEP D - Move numbers into correct places
        for(int i = 0; i < a.length; i++) {
            b[digitPointers[(a[i] >>> shift) & mask]++] = a[i];
        }
    }

}
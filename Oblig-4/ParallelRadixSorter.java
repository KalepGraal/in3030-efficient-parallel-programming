import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallelRadixSorter implements Sorter {
    private final int useBits;      // How many bits represents a digit/bucket for the radix sorting

    private final int numThreads;           // Number of threads we are using
    private ExecutorService threadPool;     // ThreadPool that handles running our threads
    private final CyclicBarrier barrier;    // Barrier for main thread to wait for workers

    private int[] a;        // Array we're currently sorting

    ParallelRadixSorter(int useBits, int numThreads) {
        this.useBits = useBits;
        this.numThreads = numThreads;
        barrier = new CyclicBarrier(numThreads + 1);
    }

    public int[] sort(int[] arrToSort) {
        a = arrToSort;
        sort();
        return a;
    }

     private void sort() {
        // Create a threadPool for the number of threads we need
        threadPool = Executors.newFixedThreadPool(numThreads);

        WorkerCollection workers = new WorkerCollection(a.length);

        // Step A: Find the max value
        int max = workers.findMax();

        // Then we need to know how many bits the max value needs
        int bitsNeeded = 1;
        while (max >= (1L << bitsNeeded)) bitsNeeded ++;

        // Then we find out how many (max needed) digits we have when each digit uses useBits bits
        int numDigits = Math.max(1, bitsNeeded/useBits);

        // Next we need to know how many digits each sorting step will use
        int[] bit = new int[numDigits];

        // Since we could have an uneven amount, we also need to distribute the rest
        int rest = bitsNeeded % numDigits;

        // Do the actual distributing
        for (int i = 0; i < bit.length; i++) {
            bit[i] = bitsNeeded/numDigits;
            if (rest-- > 0) bit[i]++;
        }

        // Now that we know how many bits we will use in each step, we can start the actual sorting
        int[] b = new int[a.length];  // array used when moving digits into correct position
        int shift = 0;  // keeps track of where the digit we're sorting on starts
        for (int i = 0; i < numDigits; i++) {
            // Step B: Count the frequency of each digit
            workers.countDigitFreq(bit[i], shift);

            // Step C and D: Move the numbers into the correct position
            workers.moveNumbersToNewPos(b, bit[i], shift);

            // Increment the bit shift
            shift += bit[i];

            // Swap a and b
            int[] tmp = a; a = b; b = tmp;
        }

        // Array should now be sorted, will no longer need our worker threads
        threadPool.shutdown();
    }



    class Slice {
        // Represents a index range a worker works on
        final int from;
        final int to;

        Slice(int from, int to) {
            this.from = from;
            this.to = to;
        }
    }

    private void awaitBarrier(CyclicBarrier barrier) {
        try {
            barrier.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    class Worker {
        final int id;         // Unique id of worker
        final Slice slice;    // Slice of total array this worker will work on
        final CyclicBarrier workersBarrier;   // Allows us to sync with the other workers

        // Results of operations:
        int maxResult;      // This worker's findMax result
        int[] sumCount;     // Reference to total count for each digit (after counting)
        int[][] allCount;   // Reference to count for each digit in each worker range


        Worker(int id, Slice slice, CyclicBarrier workersBarrier) {
            this.id = id;
            this.slice = slice;
            this.workersBarrier = workersBarrier;
        }

        void findMax() {
            // Finds the max value in this worker's slice
            // (Assumes that slice only has positive integers)
            int max = -1;
            for (int i = slice.from; i < slice.to; i++) {
                if (a[i] > max)
                    max = a[i];
            }

            this.maxResult = max;

            awaitBarrier(barrier);
        }

        void countFreq(int mask, int shift, int[][] allCount, Slice[] digits, int[] sumCount) {
            // First we need to count digit frequencies in this workers range
            int[] count = new int[mask + 1];
            for (int i = slice.from; i < slice.to; i++) {
                count[(a[i] >> shift) & mask]++;
            }

            // Update shared records
            allCount[id] = count;
            awaitBarrier(workersBarrier);

            // Now that we know digit frequencies in each range, we need to summarize these

            // Each worker summarizes on its given digit range.
            Slice myDigits = digits[id];
            for (int i = 0; i < numThreads; i++) {
                for (int j = myDigits.from; j < myDigits.to; j++) {
                    sumCount[j] += allCount[i][j];
                }
            }

            // Store reference to sumCount and allCount to use later when moving numbers
            this.sumCount = sumCount;
            this.allCount = allCount;

            // Signals that this worker is done
            awaitBarrier(barrier);
        }

        void moveNumToNewPos(int[] b, int mask, int shift) {
            int numDigits = allCount[id].length;
            int[] myStartPointers = new int[numDigits];

            // Step C: Calculate the new location of digits
            // First we find the lowest possible start position
            for (int i = 1; i < sumCount.length; i++) {
                myStartPointers[i] = sumCount[i-1] + myStartPointers[i-1];
            }

            // Then we need to account for digits found in other worker's ranges.
            // Since we're prioritizing based on the worker's range, we only need to check
            // workers with a lower range than this one, which in this case also is workers with a lower id.

            for (int i = 0; i < id; i++) {
                // Summarize occurrences of each digit
                for (int j = 0; j < allCount[i].length; j++) {
                    myStartPointers[j] += allCount[i][j];
                }
            }

            // Step D: Now we have our initial positions, and can start inserting into the array
            for (int i = slice.from; i < slice.to; i++) {
                b[myStartPointers[(a[i] >>> shift) & mask]++] = a[i];
            }

            awaitBarrier(barrier);
        }
    }


    class WorkerCollection {
        private Worker[] workers;

        WorkerCollection(int arrayLength) {
            createWorkers(arrayLength);
        }

        private void createWorkers(int arrayLength) {
            // Creates workers with equally divided indices on an array with given length
            Worker[] workers = new Worker[numThreads];
            Slice[] slices = createSlices(arrayLength, numThreads);
            CyclicBarrier workersBarrier = new CyclicBarrier(numThreads);
            for (int i = 0; i < numThreads; i++) {
                workers[i] = new Worker(i, slices[i], workersBarrier);
            }
            this.workers = workers;
        }

        private Slice[] createSlices(int arrLength, int numSlices) {
            // Helper function to divide an array between workers

            Slice[] slices = new Slice[numSlices];
            int slice = arrLength / numSlices;
            int rest = arrLength % numSlices;
            int offset = 0;
            for (int i = 0; i < numSlices; i++) {
                int low = i * slice + offset;
                int high = low + slice;

                if (rest > 0) {
                    high++; offset++; rest--;
                }
                slices[i] = new Slice(low, high);
            }
            return slices;
        }

        int findMax() {
            // Find each workers local max
            for (Worker w : workers)
                threadPool.execute(w::findMax);

            // Wait for workers to finish
            awaitBarrier(barrier);

            // Find max of workers max values
            int max = -1;
            for (Worker w : workers)
                if (w.maxResult > max) max = w.maxResult;

            return max;
        }

        void countDigitFreq(int maskLength, int shift) {
            // Array to keep count of each digit in each worker's range
            int[][] allCount = new int[numThreads][];

            // Array to keep total count of each digit
            int[] sumCount = new int[1 << maskLength];

            // Split the digits between the workers:
            Slice[] digits = createSlices(1 << maskLength, numThreads);

            // Create bit mask
            int mask = (1 << maskLength) - 1;

            // Start each worker
            for (Worker w : workers) {
                threadPool.execute(() -> w.countFreq(mask, shift, allCount, digits, sumCount));
            }

            // Wait for workers to finish
            awaitBarrier(barrier);
        }

        void moveNumbersToNewPos(int[] b, int maskLength, int shift) {
            // Create bit mask
            int mask = (1 << maskLength) - 1;

            // Moves numbers from a into b, using the new pointers given.
            for (Worker w : workers)
                threadPool.execute(() -> w.moveNumToNewPos(b, mask, shift));

            // Wait for workers to finish
            awaitBarrier(barrier);
        }


    }

}

import java.util.Arrays;

class Main {

    public static void main(String[] args) {
        if(args.length != 2) {
            System.out.println("This program takes two arguments: <n> <seed>");
            return;
        }

        int n = Integer.parseInt(args[0]);
        int seed = Integer.parseInt(args[1]);
        int timesToTime = 7;
        int useBits = 8;     // Using 8 since it gave the fastest results on my machine

        Sorter seqSorter = new SequentialRadixSorter(useBits);
        Sorter parSorter = new ParallelRadixSorter(useBits, Runtime.getRuntime().availableProcessors());

        int[] unsortedArray = Oblig4Precode.generateArray(n, seed);

        // Do timings
        TimingResult<int[]> seqTimingResult = doTimingOfSort(unsortedArray, timesToTime, seqSorter);
        TimingResult<int[]> parTimingResult = doTimingOfSort(unsortedArray, timesToTime, parSorter);

        int[] SeqSortedArray = seqTimingResult.object;
        int[] ParSortedArray = parTimingResult.object;

        if (testIsNotSorted(SeqSortedArray))
            System.out.println("Error! array not properly sorted for sequential solution");

        if (testIsNotSorted(ParSortedArray))
            System.out.println("Error! array not properly sorted for parallel solution");

        Oblig4Precode.saveResults(Oblig4Precode.Algorithm.SEQ, seed, SeqSortedArray);
        Oblig4Precode.saveResults(Oblig4Precode.Algorithm.PARA, seed, ParSortedArray);

        // Store results, so we can easily output these from another program
        String[] lastResult = new String[] {
                Integer.toString(n),
                Long.toString(seqTimingResult.timeInMs()) + " ms",
                Long.toString(parTimingResult.timeInMs()) + " ms",
                String.format("%.2f", (double)seqTimingResult.time/parTimingResult.time),
                Boolean.toString(Arrays.equals(SeqSortedArray, ParSortedArray)),
                Integer.toString(timesToTime)};


        System.out.format("%-12s%-10s%-10s%-10s%-13s%-10s\n", (Object[]) lastResult);
    }

    private static TimingResult<int[]> doTimingOfSort(int[] arr, int timesToTime, Sorter sorter) {
        // Helper function to take the timing of the sorting, but not of cloning the array

        long[] timeResults = new long[timesToTime];
        int[] result = null;

        for (int i = 0; i < timesToTime; i++) {

            int[] clone = arr.clone();

            long start = System.nanoTime();
            result = sorter.sort(clone);
            long end = System.nanoTime();

            timeResults[i] = end - start;
        }

        Arrays.sort(timeResults);
        long medianTime = timeResults[timesToTime/2];

        return new TimingResult<>(medianTime, result);
    }

    private static boolean testIsNotSorted(int[] a) {
        for (int i = 1; i < a.length; i++) {
            if (a[i-1] > a[i]) {
                System.out.printf("IsSorted check failed at i=%d\n", i);
                return true;
            }
        }
        return false;
    }


    /**
     * @param <R> Result type
     */
    static class TimingResult<R> {
        final long time;
        final R object;

        /**
         * @param time how much time the timing took
         * @param object result of the operation timing was done on
         */
        TimingResult(long time, R object) {
            this.time = time;
            this.object = object;
        }

        long timeInMs() {
            return time / 1_000_000;
        }

    }
}

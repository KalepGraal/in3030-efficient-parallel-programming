interface Sorter {
    int[] sort(int[] unsortedArray);
}

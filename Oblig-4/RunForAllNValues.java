class RunForAllNValues {
    /**  Runs our main program with the different values of N*/
    public static void main(String[] args) {
        int seed = 42;
        System.out.printf("Running timings for all values of n, with the seed: %d\n\n", seed);
        System.out.format("%-12s%-10s%-10s%-10s%-13s%-10s\n", "n", "Seq", "Para", "Speedup", "Same Result", "Times ran");
        for (long i = 1_000; i <= 100_000_000; i *= 10) {
            String[] arguments = new String[]{Long.toString(i), Integer.toString(seed)};
            Main.main(arguments);
        }
    }
}

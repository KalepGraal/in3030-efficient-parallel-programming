import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

class ParSieve {
    /**
     *  Parallelized version of the Sieve of Eratosthenes
     *  Based on the sequential solution handout.
     */

    private final int n;
    private int squaredN;
    private int numThreads;
    private int firstPrimesCount;
    private byte[] bytes;
    private CyclicBarrier cyclicBarrier;
    HashMap<Long, List<Long>> factorizedNumbers;

    List<Long> timingsSieve         = new ArrayList<>();
    List<Long> timingsFactorization = new ArrayList<>();

    ParSieve(int N, int k) {

        this.n = N;
        squaredN = (int) Math.sqrt(n);

        if (k > 0)
            numThreads = k;
        else {
            numThreads = Runtime.getRuntime().availableProcessors();
        }

        cyclicBarrier = new CyclicBarrier(numThreads + 1);
    }

    int[] start() {
        long start = System.currentTimeMillis();
        int[] primes = startPrimeFinding();
        timingsSieve.add(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        ParFactorize pf = new ParFactorize(primes, numThreads);
        factorizedNumbers = pf.factorizeRange((long) n*n-100, (long) n*n);
        timingsFactorization.add(System.currentTimeMillis() - start);

        return primes;
    }

    int[] startPrimeFinding(){
        // resetting to default state here, to allow for multiple runs (timings)
        firstPrimesCount = 0;
        bytes = new byte[n / 16 + 1];   // add 1 because of Integer rounding

        // First the main thread finds the first sqrt(n) primes
        findFirstPrimes();

        // Then we create and start our worker threads
        Worker[] workers = createWorkers();

        for (Worker w : workers)
            new Thread(w).start();

        // wait until workers finish
        waitForBarrier();

        // since we counted primes in each worker segment, we can now update the total primeCount
        int primesInSegments = Arrays.stream(workers)
                                     .mapToInt(x -> x.segmentPrimeCount)
                                     .sum();

        int totalPrimeCount = primesInSegments + firstPrimesCount;

        return collectWorkersPrimes(workers, totalPrimeCount);
    }

    /** Finds the first primes up to SquaredN*/
    private void findFirstPrimes(){
        int currentPrime = 3;
        firstPrimesCount++;

        while(currentPrime <= squaredN){
            traverseInSegment(currentPrime, 0, squaredN+1);
            currentPrime = findNextPrime(currentPrime + 2);
            firstPrimesCount++;
        }

        // totalPrimeCount = firstPrimesCount;
    }

    /**
     * Waits for all threads to finish before continuing.
     */
    private void waitForBarrier() {
        try {
            cyclicBarrier.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void traverseInSegment(int prime, int low, int high){
        int start = prime*prime;

        // ensure we're in our segment
        while (start < low)
            start += prime * 2;

        for (int i = start; i < high; i += prime * 2){
            flip(i);
        }
    }


    private void flip(int i){
        int byteCell = i / 16;  // find respective cell (index) in byte[] bytes
        int bit = (i / 2) % 8;  // find respective bit within byteIndex

        /*
         * 1 << bit shifts a binary 1 bit number of spaces to the left in the byte.
         * 1 << 0 = 0001
         * 1 << 1 = 0010
         * 1 << 2 = 0100
         * 1 << 3 = 1000, etc.
         *
         * | is the OR operand where we basically switch the relevant bit to a 1.
         * |= works similarly to +=
         * */
        bytes[byteCell] |= (1 << bit);
    }


    private boolean isPrime(int i){

        int byteCell = i / 16;
        int bit = (i / 2) % 8;

        /*
         * see bit shift explanation in flip(), same thing here.
         * difference is instead of OR we have an AND operand, which in this case is used to compare
         * your respective bit to a flipped bit, meaning it returns 0 if it hasn't been flipped (is a prime)
         * and > 0 if it has been flipped.
         * */
        return (bytes[byteCell] & (1 << bit)) == 0;
    }


    private int findNextPrime(int p){
        for (int i = p; i < n; i += 2){
            if (isPrime(i))
                return i;
        }
        return 0;
    }


    private class Worker implements Runnable {
        int low, high;              // byte-indexes used
        int firstReal, lastReal;    // actual numbers to check
        int segmentPrimeCount = 0;
        int[] primes;

        Worker(int low, int high) {
            this.low = low;
            this.high = high;

            firstReal = Math.max(low*16, squaredN+1);
            lastReal = Math.min(high*16, n);

            if (firstReal % 2 == 0){
                firstReal++;
            }
        }

        @Override
        public void run() {

            sieveInSegment();
            countPrimesInSegment();
            findPrimesInSegment();

            waitForBarrier();
        }

        /** Uses the first primes we found, to mark non-primes in this workers segment*/
        private void sieveInSegment() {
            int currentPrime = 3;
            for (int i = 1; i < firstPrimesCount; i++){
                // check that we're still inside our segment
                if (currentPrime*currentPrime > lastReal) break;
                traverseInSegment(currentPrime, firstReal, lastReal);
                currentPrime = findNextPrime(currentPrime + 2);
            }
        }

        /** Counts the number of primes in this workers segment*/
        private void countPrimesInSegment() {
            int currentPrime = findNextPrime(firstReal);

            while (currentPrime != 0){
                segmentPrimeCount++;
                currentPrime = findNextPrimeInSegment(currentPrime + 2);
            }
        }

        /** Collects the primes in this workers segment*/
        private void findPrimesInSegment() {
            primes = new int[segmentPrimeCount];

            int currentPrime = findNextPrime(firstReal);
            for (int i = 0; i < segmentPrimeCount; i++) {
                primes[i] = currentPrime;
                currentPrime = findNextPrime(currentPrime + 2);
            }
        }

        private int findNextPrimeInSegment(int p){
            for (int i = p; i < lastReal; i += 2){
                if (isPrime(i))
                    return i;
            }
            return 0;
        }

    }

    private Worker[] createWorkers() {
        Worker[] workers = new Worker[numThreads];

        // When creating our segments, we find the bytes each segment should use
        // this ensures no two workers ever write to the same byte

        int startByte = squaredN/16;

        // Find out how large each segment should be
        int segment_size = (bytes.length - startByte) / numThreads;
        int rest = (bytes.length - startByte) % numThreads;

        int offset = startByte;
        for (int i = 0; i < numThreads; i++) {

            int low = i * segment_size + offset;
            int high = low + segment_size;

            if (rest > 0) {
                high++;
                offset++;
                rest--;
            }

            workers[i] = new Worker(low, high);
        }
        return workers;
    }

    private int[] collectWorkersPrimes(Worker[] workers, int totalPrimeCount) {
        // first we get the first primes
        int[] primes = new int[totalPrimeCount];
        primes[0] = 2;

        int currentPrime = 3;
        int i;
        for (i = 1; i < firstPrimesCount; i++) {
            primes[i] = currentPrime;
            currentPrime = findNextPrime(currentPrime + 2);
        }

        // then we get the ones our workers found
        int offset = firstPrimesCount;
        for (Worker w : workers) {
            System.arraycopy(w.primes, 0, primes, offset, w.segmentPrimeCount);
            offset += w.segmentPrimeCount;
        }

        return primes;
    }

}

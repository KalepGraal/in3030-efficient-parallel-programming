import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Main {

    private static final int timesToTime = 7;

    public static void main(String[] args) {
        int N = 0, k = 0, threads = 0;
        TimeTaker.TimingResult<int[]> sequential;
        TimeTaker.TimingResult<int[]> parallel;

        long seqTimeSieve, seqTimeFac;
        long parTimeSieve, parTimeFac;

        if (args.length < 2) {
            printUsageAndExit();
        }

        try {
            N = Integer.parseInt(args[0]);
            k = Integer.parseInt(args[1]);

            if (N < 16) {
                System.out.println("N must be 16 or larger!");
                System.exit(1);
            }
            threads = k == 0 ? Runtime.getRuntime().availableProcessors() : k;

        } catch (NumberFormatException ex) {
            System.out.println("invalid arguments: " + Arrays.toString(args));
            printUsageAndExit();
        }

        System.out.printf("Running Main with N=%d, k=%d (%d threads)\n", N, k, threads);
        System.out.printf("Timings are the median values of %d runs\n\n", timesToTime);

        // Do prime-finding and factorization for sequential solution
        SequentialSieve SS = new SequentialSieve(N);
        sequential = TimeTaker.time(timesToTime, SS::start);

        // Do prime-finding and factorization for parallel solution
        ParSieve PS = new ParSieve(N, threads);
        parallel = TimeTaker.time(timesToTime, PS::start);


        // Retrieve median of inner timings
        SS.timingsSieve.sort(Comparator.naturalOrder());
        SS.timingsFactorization.sort(Comparator.naturalOrder());
        PS.timingsSieve.sort(Comparator.naturalOrder());
        PS.timingsFactorization.sort(Comparator.naturalOrder());

        seqTimeSieve = SS.timingsSieve.get(timesToTime/2);
        seqTimeFac   = SS.timingsFactorization.get(timesToTime/2);
        parTimeSieve = PS.timingsSieve.get(timesToTime/2);
        parTimeFac   = PS.timingsFactorization.get(timesToTime/2);


        // Test that both sequential and parallel produced same primes
        if (!Arrays.equals(sequential.object, parallel.object)) {
            System.out.println("ERROR: sequential and parallel produced different primes!");

            for (int i = 0; i < sequential.object.length; i++) {
                if (sequential.object[i] != parallel.object[i]) {
                    System.out.printf("Differs at %d, %d != %d\n", i, sequential.object[i], parallel.object[i]);
                    break;
                }
            }
        }

        // Test that the sequential and parallel produced the same factors

        for (Long number : PS.factorizedNumbers.keySet()) {
            if (!Arrays.equals(PS.factorizedNumbers.get(number).toArray(),
                              SS.factorizedNumbers.get(number).toArray())) {
                System.out.println("ERROR: Factorizations did not produce the same result!");
            }
        }


        // Write primes from parallel solution to file
        writeFactorsToFile(N, PS.factorizedNumbers);


        // Write timing results to terminal
        String[] Sieve = new String[] {"Sieve",
                                        Integer.toString(N),
                                        Long.toString(seqTimeSieve) + " ms",
                                        Long.toString(parTimeSieve) + " ms",
                                        String.format("%.2f", (double)seqTimeSieve/parTimeSieve)};

        String[] Factorization = new String[] {"Factorization",
                                                Integer.toString(N),
                                                Long.toString(seqTimeFac) + " ms",
                                                Long.toString(parTimeFac) + " ms",
                                                String.format("%.2f", (double)seqTimeFac/parTimeFac)};

        String[] Total = new String[] {"Total",
                                        Integer.toString(N),
                                        Long.toString(sequential.timeInMs()) + " ms",
                                        Long.toString(parallel.timeInMs()) + " ms",
                                        String.format("%.2f", (double)sequential.timeInMs()/parallel.timeInMs())};

        System.out.format("%-15s%-13s%-10s%-10s%-7s\n", "", "n", "Seq", "Para", "Speedup");
        System.out.format("%-15s%-13s%-10s%-10s%-7s\n", (Object[]) Sieve);
        System.out.format("%-15s%-13s%-10s%-10s%-7s\n", (Object[]) Factorization);
        System.out.format("%-15s%-13s%-10s%-10s%-7s\n\n", (Object[]) Total);

    }

    private static void writeFactorsToFile(int n, HashMap<Long, List<Long>> factors) {
        Oblig3Precode o3p = new Oblig3Precode(n);

        factors.forEach((base, values)
                -> values.forEach(value -> o3p.addFactor(base, value)));

        o3p.writeFactors();
    }

    private static void printTimingResult(TimeTaker.TimingResult<int[]> result, String name) {
        System.out.printf("%s median exec time: %d ms (ran %d times)\n", name, result.timeInMs(), timesToTime);
        if (result.object.length < 200)
            System.out.println("Par: " + Arrays.toString(result.object));
        System.out.println("Length: " + result.object.length);
        System.out.println("First 5 primes: " + Arrays.toString(Arrays.copyOf(result.object, 5)));
        System.out.printf(" Last 5 primes: %s\n\n", Arrays.toString(Arrays.copyOfRange(result.object, result.object.length -6, result.object.length-1)));
    }

    private static void printUsageAndExit() {
        System.out.println("Correct usage: \"java Main N k\"");
        System.out.println("N: number greater than 16 of to generate primes below");
        System.out.println("k: number of threads to use. (0 defaults to available processors)");

        System.exit(1);
    }




}

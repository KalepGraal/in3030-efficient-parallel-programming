import java.util.Arrays;
import java.util.function.Supplier;

class TimeTaker {

    private final static boolean DEBUG = false;

    /**
     * IMPORTANT: This assumes function result is always the same
     *
     * @param supplier function to do timing on
     * @param <R> result type of function
     * @return TimingResult with time taken and result from function
     */
    static <R> TimingResult<R> time(int timesToRun, Supplier<R> supplier) {

        long[] timeResults = new long[timesToRun];
        R result = null;

        for (int i = 0; i < timesToRun; i++) {
            long start = System.nanoTime();

            result = supplier.get();
            long end = System.nanoTime();

            timeResults[i] = end - start;
            if (DEBUG) System.out.printf("time[%d]: %d\n", i, timeResults[i]);


        }

        Arrays.sort(timeResults);
        long medianTime = timeResults[timesToRun/2];

        if (DEBUG) System.out.println("Median: " + medianTime);


        return new TimingResult<>(medianTime, result);
    }

    /**
     * @param <R> Result type
     */
    static class TimingResult<R> {
        long time;
        R object;

        /**
         * @param time how much time the timing took
         * @param object result of the operation timing was done on
         */
        TimingResult(long time, R object) {
            this.time = time;
            this.object = object;
        }

        long timeInMs() {
            return time / 1_000_000;
        }

        long timeInSeconds() {
            return time / 1_000_000_000;
        }

    }

}

import java.util.*;
import java.util.concurrent.CyclicBarrier;

public class ParFactorize {

    private int[] primes;
    private int numWorkers;
    private Worker[] workers;
    private long currentNum;
    private CyclicBarrier cyclicBarrier;
    private List<Long> factors;

    ParFactorize(int[] primes, int numWorkers){
        workers = new Worker[numWorkers];
        this.primes = primes;
        this.numWorkers = numWorkers;
    }

    HashMap<Long, List<Long>> factorizeRange(long from, long to) {
        HashMap<Long, List<Long>> map = new HashMap<>();

        for (long i = from; i < to; i++) {
            map.put(i, factorize(i));
        }

        return map;
    }

    private List<Long> factorize(long num) {
        factors = new ArrayList<>();
        currentNum = num;

        workers = new Worker[numWorkers];
        cyclicBarrier = new CyclicBarrier(numWorkers+1);

        for (int i = 0; i < numWorkers; i++)
            workers[i] = new Worker(i);

        for (Worker w : workers)
            new Thread(w).start();

        // wait for worker threads to finish
        try { cyclicBarrier.await(); } catch (Exception ex) {ex.printStackTrace();}

        if (currentNum != 1) {
            // if the current num is not 1, means that none of the workers
            // found a factor that fit, and therefore it must be a prime
            factors.add(currentNum);
        }

        factors.sort(Comparator.naturalOrder());

        return factors;
    }

    synchronized boolean tryAddFactor(long factor, long workers_num) {
        // Probably fine to use synchronized here, as we won't be doing
        // this operation a lot
        if (workers_num == currentNum) {
            factors.add(factor);
            currentNum = currentNum / factor;
            return true;
        }

        return false;
    }

    private class Worker implements Runnable {
        int id;
        boolean isUpdated = true;

        Worker(int id) {
            this.id = id;
        }

        @Override
        public void run() {
            long num = currentNum;
            int roof = (int) Math.sqrt(num) + 1;

            for (int i = id; i < primes.length && primes[i] < roof; i+=numWorkers) {
                if (num % primes[i] == 0) {
                    // we found a factor, try to add it
                    if (tryAddFactor(primes[i], num)) {
                        // factor was successfully added
                        for (Worker w : workers) {
                            // tell all other workers to update their number to factorize
                            if (w != this) w.isUpdated = false;
                        }
                        num = num / primes[i];
                        roof = (int) Math.sqrt(num) + 1;
                    } else {
                        // we could not add factor, which means another worker added one
                        // therefore we must update the number to factorize, and try again
                        isUpdated = false;
                    }
                    i-=numWorkers; // so that we check this number again
                }

                // Check if another worker found a new factor
                if (!isUpdated) {
                    // its fine to not use any synchronization on isUpdated, since only the worker
                    // itself sets it to true, so even if two other threads modify it at the same time,
                    // we know they both will set it to false.
                    num = currentNum;
                    roof = (int) Math.sqrt(num) + 1;
                    isUpdated = true;
                }
            }

            try { cyclicBarrier.await(); } catch (Exception ex) {ex.printStackTrace();}
        }

    }



}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SeqFactorize {

    private int[] primes;

    SeqFactorize(int[] primes){
        this.primes = primes;
    }


    HashMap<Long, List<Long>> factorizeRange(long from, long to) {
        HashMap<Long, List<Long>> map = new HashMap<>();

        for (long i = from; i < to; i++) {
            map.put(i, factorize(i));
        }

        return map;
    }

    private List<Long> factorize(long num) {
        ArrayList<Long> factors = new ArrayList<>();

        long tmp = num;

        while (tmp != 1) {
            long factor = findFactor(tmp);
            factors.add(factor);
            tmp = tmp / factor;
        }
        return factors;
    }

    private long findFactor(long num) {
        int roof = (int) Math.sqrt(num) + 1;
        for (int prime : primes){
            if (prime >= roof) break;
            if (num % prime == 0) {
                return prime;
            }
        }
        // number is a prime (provided we have enough primes in our primes array)
        return num;
    }

}

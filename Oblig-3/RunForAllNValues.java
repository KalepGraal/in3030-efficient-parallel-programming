public class RunForAllNValues {
    /**  Runs our main program with the different values of N*/
    public static void main(String[] args) {
        for (long i = 2_000_000; i <= 2_000_000_000; i *= 10) {
            String[] arguments = new String[]{Long.toString(i), "0"};
            Main.main(arguments);
        }
    }
}

import java.util.Arrays;

public class TestResults {

    /** These tests assume output from Arrays.Sort is correct,
     * not ideal, but works for my needs
     * */
    public static void main(String[] args) {

        int n = 100000000;
        int k = 100;

        int[] arr = Main.createArrayToSort(n);
        System.out.println(String.join(", ", Arrays.toString(arr)));

        System.out.println("ArraySort");
        int[] arrToSort = arr.clone();
        Sorter s = new Sorter(arrToSort, SortType.ArraysSort);
        int[] arraysort = s.getKHighest(k);
        System.out.println(String.join(", ", Arrays.toString(arraysort)));

        System.out.println("InsertSort");
        arrToSort = arr.clone();
        s = new Sorter(arrToSort, SortType.InsertSort);
        int[] kToTest = s.getKHighest(k);
        System.out.println(String.join(", ", Arrays.toString(kToTest)));
        assert Arrays.equals(arraysort, kToTest);

        System.out.println("Parallelized ArraySort");
        arrToSort = arr.clone();
        Parallelized p = new Parallelized();
        kToTest = p.getKHighest(arrToSort, k, SortType.ArraysSort);
        System.out.println(String.join(", ", Arrays.toString(kToTest)));
        assert Arrays.equals(arraysort, kToTest);

        System.out.println("Parallelized InsertSort");
        arrToSort = arr.clone();
        p = new Parallelized();
        kToTest = p.getKHighest(arrToSort, k, SortType.InsertSort);
        System.out.println(String.join(", ", Arrays.toString(kToTest)));
        assert Arrays.equals(arraysort, kToTest);
    }


}

class TimingResult {

    int n;
    long time;
    TimingResult(int n, long time) {
        this.n = n;
        this.time = time;
    }

    void print() {
        System.out.printf("%s%-9d%9s%d\n", "n = ", n, "time: ", time);
    }

}
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {

    static final boolean DEBUG = false;
    static final boolean DEBUG_THREADS = false;

    public static void main(String[] args) {

        if (args.length > 0){
            int n = Integer.parseInt(args[0]);
            int k = Integer.parseInt(args[1]);
            doTimings(n, k);
        } else {
            doAllTimings();
        }

    }

    private static void doTimings(int n, int k) {
        int[] arr = createArrayToSort(n);

        System.out.println("Arrays.sort():");
        takeMedianTimeOf(arr, n, k, SortType.ArraysSort, false).print();

        System.out.println("\ninsertSort():");
        takeMedianTimeOf(arr, n, k, SortType.InsertSort, false).print();

        System.out.println("\nParallelized Arrays.sort():");
        takeMedianTimeOf(arr, n, k, SortType.ArraysSort, true).print();

        System.out.println("\nParallelized insertSort():");
        takeMedianTimeOf(arr, n, k, SortType.InsertSort, true).print();

    }

    private static void doAllTimings() {
        List<TimingResult> arraySort = new ArrayList<>();
        List<TimingResult> insertSort = new ArrayList<>();
        List<TimingResult> parArraySort = new ArrayList<>();
        List<TimingResult> parInsertSort = new ArrayList<>();

        int k = 100;
        for (int n = 1000; n <= 100000000; n*=10) {
            System.out.println(String.format("###### Running timings for n=%s, k=%s\n", n, k));

            int[] arr = createArrayToSort(n);

            System.out.println("Arrays.sort():");
            arraySort.add(takeMedianTimeOf(arr, n, k, SortType.ArraysSort, false));

            System.out.println("\ninsertSort():");
            insertSort.add(takeMedianTimeOf(arr, n, k, SortType.InsertSort, false));

            System.out.println("\nParallelized Arrays.sort():");
            parArraySort.add(takeMedianTimeOf(arr, n, k, SortType.ArraysSort, true));

            System.out.println("\nParallelized insertSort():");
            parInsertSort.add(takeMedianTimeOf(arr, n, k, SortType.InsertSort, true));

            System.out.println();
        }

        System.out.println("Results Arrays.sort()");
        for (TimingResult tr : arraySort)
            tr.print();

        System.out.println("\nResults insertSort()");
        for (TimingResult tr : insertSort)
            tr.print();

        System.out.println("\nResults Parallelized Arrays.sort()");
        for (TimingResult tr : parArraySort)
            tr.print();

        System.out.println("\nResults Parallelized insertSort()");
        for (TimingResult tr : parInsertSort)
            tr.print();

        exportToCsv("results/arraySort.csv", arraySort);
        exportToCsv("results/insertSort.csv", insertSort);
        exportToCsv("results/parArraySort.csv", parArraySort);
        exportToCsv("results/parInsertSort.csv", parInsertSort);

    }

    static int[] createArrayToSort(int n) {
        return new Random().ints(n).toArray();
    }

    private static TimingResult takeMedianTimeOf(int[] arr, int n, int k, SortType sortType, boolean useParallel) {
        long[] times = new long[7];
        long start, end;
        int[] arrToSort;

        int[] kHighest = new int[k];

        for (int i = 0; i < 7; i++) {

            if (Main.DEBUG) System.out.printf("%d/%d Starting... ", i, 7);
            // Ensures we're actually sorting the unsorted array
            arrToSort = arr.clone();

            if (useParallel) {
                Parallelized p = new Parallelized();
                start = System.nanoTime();
                kHighest = p.getKHighest(arrToSort, k, sortType);
                end = System.nanoTime();

            } else {
                Sorter s = new Sorter(arrToSort, sortType);
                start = System.nanoTime();
                kHighest = s.getKHighest(k);
                end = System.nanoTime();
            }
            times[i] =  end - start;
            if (Main.DEBUG) System.out.printf("Done in %d\n", times[i]);
        }

        // find median of times ran
        Arrays.sort(times);
        long median = times[3];

        System.out.println("median time: " + median);

        System.out.println(Arrays.toString(kHighest));

        return new TimingResult(n, median);
    }

    private static void exportToCsv(String filename, List<TimingResult> results) {
        try {
            File file = new File(filename);
            file.getParentFile().mkdirs();
            FileWriter fw = new FileWriter(file);
            fw.write("n,time\n");
            for (TimingResult tr : results) {
                fw.write(String.format("%s,%s\n", tr.n, tr.time));
            }
            fw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

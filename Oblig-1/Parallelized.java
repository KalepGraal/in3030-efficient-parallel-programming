import java.util.Arrays;
import java.util.concurrent.CyclicBarrier;

public class Parallelized {

    private CyclicBarrier barrier;
    private static int workerCount = 0;

    int[] getKHighest(int[] arr, int k, SortType sortType) {

        int numProcessors = Runtime.getRuntime().availableProcessors();
        if (Main.DEBUG_THREADS) System.out.println("numProcessors: " + numProcessors);

        barrier = new CyclicBarrier(numProcessors+1);
        Worker[] workers = new Worker[numProcessors];

        int slices = arr.length/numProcessors;
        int rest = arr.length - slices*numProcessors;
        int offset = 0;

        if (Main.DEBUG_THREADS) System.out.println("slices size: " + slices);
        if (Main.DEBUG_THREADS) System.out.println("rest: " + rest);

        for (int i = 0; i < numProcessors; i++) {

            int from = i * slices + offset;
            int upto = from + slices;

            // ensure that slices are distributed evenly
            if (rest > 0) {
                upto+=1;
                offset++;
                rest--;
            }

            Sorter sorter = new Sorter(arr, from, upto, sortType);

            workers[i] = new Worker(sorter, k, from, upto);
            new Thread(workers[i]).start();
        }

        try {
            barrier.await();
            if (Main.DEBUG_THREADS) System.out.println("All threads finished!");
            workerCount = 0;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Now we want to collect all KHighest from the workers and sort these,
        // to find the collective KHighest

        int[] WorkersKHighest = new int[k*numProcessors];

        int destIndex = 0;
        for (int i = 0; i < numProcessors; i++) {
            if (Main.DEBUG_THREADS) {
                System.out.printf("--- [%d] %d - %d\n", workers[i].workerId, workers[i].from, workers[i].upto);
                System.out.println(String.join(", ", Arrays.toString(workers[i].KHighest)));
            }

            int numToCpy = k < workers[i].slice ? k : workers[i].slice;

            System.arraycopy(workers[i].KHighest, 0, WorkersKHighest, destIndex, numToCpy);
            destIndex+=numToCpy;
        }

        if (Main.DEBUG_THREADS) System.out.println("worker threads combined: ");
        if (Main.DEBUG_THREADS) System.out.println(String.join(", ", Arrays.toString(WorkersKHighest)));

        Sorter s = new Sorter(WorkersKHighest, 0, WorkersKHighest.length, sortType);

        int[] KHighest = s.getKHighest(k);

        if (Main.DEBUG_THREADS) System.out.println("final result: ");
        if (Main.DEBUG_THREADS) System.out.println(String.join(", ", Arrays.toString(KHighest)));

        return KHighest;
    }

    class Worker implements Runnable {
        int workerId;
        int k, from, upto, slice;

        Sorter sorter;
        int[] KHighest;

        Worker(Sorter sorter, int k, int from, int upto) {
            this.workerId = ++workerCount;
            this.sorter = sorter;
            this.k = k;
            this.from = from;
            this.upto = upto;
            this.slice = upto - from;
        }

        @Override
        public void run() {
            if (Main.DEBUG_THREADS) {
                int[] arr = sorter.getArrayToSort();
                System.out.printf("worker %d [%d-%d] started!: " + Arrays.toString(Arrays.copyOfRange(arr, from, upto))+"\n", workerId, from, upto);
            }

            // when n is low, we can get a situation where each slice is larger than k...
            if (slice < k) {
                if (Main.DEBUG_THREADS) System.out.printf("workers slice is lower than k, slice: %d, k: %d\n",  upto - from, k);
                KHighest = sorter.getKHighest(slice);
            } else {
                KHighest = sorter.getKHighest(k);
            }

            try {
                if (Main.DEBUG_THREADS) System.out.printf("worker %d finished!\n", workerId);
                barrier.await();

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

    }



}

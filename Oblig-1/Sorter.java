import java.util.Arrays;

class Sorter {

    private int [] a;
    private int v, h;
    private SortType st;

    Sorter(int[] a, int v, int h, SortType st) {
        this.a = a;
        this.v = v;
        this.h = h;
        this.st = st;
    }

    Sorter(int[] a, SortType st) {
        this.a = a;
        this.v = 0;
        this.h = a.length;
        this.st = st;
    }

    /** Returns the K highest numbers in array within previously given frame [v->h] */
    int[] getKHighest(int k) {
        int[] KHighest = new int[k];

        if (st == SortType.ArraysSort) {
            Arrays.sort(a, v, h);
            for (int i = 0; i < k; i++) {
                KHighest[i] = a[h-1-i];
            }
        } else {
            // insertSort the first k..
            insertSort(v, v+k);

            // find number larger than the smallest in the K largest so far
            for (int i = v+k; i < h; i++) {
                if (a[i] > a[k-1]) {
                    // replace smalles with new number
                    a[v+k-1] = a[i];

                    // find position for new number
                    int t = a[v+k-1];
                    int j = v+k-2;

                    while ( j >= v && a[j] < t ) {
                        a [j+1] = a [j];
                        j--;
                    }
                    a[j+1] = t;
                }
            }

            // put the K first numbers into KHighest
            System.arraycopy(a, v, KHighest, 0, k);
        }

        return KHighest;
    }

    int[] getArrayToSort() {
        return a;
    }

    /** Uses insertion sort to sort an from from index v until index h */
    private void insertSort(int v, int h) {
        int i , t ;
        for ( int k = v ; k < h-1 ; k++) {

            t = a [k+1] ;
            i = k ;
            while ( i >= v && a [i] < t ) {
                a [i+1] = a [i];
                i--;
            }
            a[i+1] = t;
        }
    }
}

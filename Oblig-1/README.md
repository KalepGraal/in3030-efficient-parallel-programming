### Running the program
The program can be ran in one of two ways:
1. Run it as `java Main <n> <k>`, which will do the timing tests for n and k. 
2. Run it without parameters, this will run all the timing tests for n = 1000 up to 100.000.000 and k = 20,100.
(Should also include -Xmx6000m to increase memory cap).

import java.util.concurrent.CyclicBarrier;

class ParallelMatrix {

    private CyclicBarrier cyclicBarrier;

    /**
     * Multiplies two matrices in parallel
     *
     * @param a matrix to multiply with
     * @param b matrix to multiply with
     * @return result of a multiplied with b
     */
    Matrix multiply(Matrix a, Matrix b) {

        int numProcessors = Runtime.getRuntime().availableProcessors();
        cyclicBarrier = new CyclicBarrier(numProcessors+1);
        Worker[] workers = new Worker[numProcessors];

        int n = a.array.length;
        int slices = n/numProcessors;
        int rest = n - slices*numProcessors;
        int offset = 0;

        Matrix result = new Matrix(new double[n][n]);

        for (int i = 0; i < numProcessors; i++) {

            int from = i * slices + offset;
            int upto = from + slices;

            // ensure that slices are distributed evenly
            if (rest > 0) {
                upto++; offset++; rest--;
            }

            workers[i] = new Worker(i, from, upto, a, b, result);
            new Thread(workers[i]).start();
        }

        waitForBarrier();

        return result;
    }

    /**
     * Waits for all threads to finish before continuing.
     */
    private void waitForBarrier() {
        try {
            cyclicBarrier.await();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Worker class for doing the multiplication for its part of the matrix
     */
    private class Worker implements Runnable {
        private int id, from, to;

        Matrix a, b, result;

        /**
         * @param id id of worker
         * @param from start pos in matrix
         * @param to end pos in matrix
         * @param a matrix to multiply with b
         * @param b matrix to multiply with a
         * @param result reference to matrix in which result will be stored*/
        Worker(int id, int from, int to, Matrix a, Matrix b, Matrix result) {
            this.id = id;
            this.from = from;
            this.to = to;
            this.a = a;
            this.b = b;
            this.result = result;
        }

        /**
         * Calculates result of multiplication of its designated part of the matrices
         */
        @Override
        public void run() {
            int n = a.array.length;

            final boolean aTransposed = a.isTransposed;
            final boolean bTransposed = b.isTransposed;

            for(int i=from;i<to;i++)
                for(int j=0;j<n;j++)
                    for(int k=0;k<n;k++)
                        result.array[i][j] += (aTransposed ? a.array[k][i] : a.array[i][k]) * (bTransposed ? b.array[j][k] : b.array[k][j]);

            waitForBarrier();
        }
    }

}

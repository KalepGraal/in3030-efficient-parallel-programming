import java.util.Arrays;
import java.util.HashMap;

public class Main {

    private final static boolean DEBUG = false;

    public static void main(String[] args) {

        int seed;
        int[] nArray;

        if (args.length < 2) {
            // if args are not supplied, we're just running all the tests
            seed = 42;
            nArray = new int[]{100, 200, 500, 1000};
        } else {
            seed = Integer.parseInt(args[0]);
            nArray = new int[]{Integer.parseInt(args[1])};
        }

        // times to do each timing, to get accurate results (due to JIT etc.)
        int times = 7;

        HashMap<Integer, HashMap<Oblig2Precode.Mode, double[][]>> results = new HashMap<>();

        for (int n : nArray) {
            HashMap<Oblig2Precode.Mode, double[][]> modeResults = new HashMap<>();
            for (Oblig2Precode.Mode mode : Oblig2Precode.Mode.values()) {
                System.out.printf("n = %d, mode: %s\n", n, mode);
                double[][] matrix = doMultiplication(seed, n, times, mode);

                modeResults.put(mode, matrix);

            }
            results.put(n, modeResults);
        }

        if (verifyResults(results)) {
            System.out.println("All results were successfully verified");
        } else {
            System.out.println("Verification of results failed");
        }
    }

    /**
     * @param seed used to generate matrices
     * @param n size of matrices
     * @param timesToRun odd number of times to run timings
     * @param mode mode to use for multiplication
     * @return double[][] containing the result matrix
     */
    private static double[][] doMultiplication(int seed, int n, int timesToRun, Oblig2Precode.Mode mode) {

        final Matrix a = new Matrix(Oblig2Precode.generateMatrixA(seed, n));
        final Matrix b = new Matrix(Oblig2Precode.generateMatrixB(seed, n));

        if (mode == Oblig2Precode.Mode.SEQ_A_TRANSPOSED || mode == Oblig2Precode.Mode.PARA_A_TRANSPOSED)
            a.transpose();

        if (mode == Oblig2Precode.Mode.SEQ_B_TRANSPOSED || mode == Oblig2Precode.Mode.PARA_B_TRANSPOSED)
            b.transpose();

        if (mode == Oblig2Precode.Mode.PARA_NOT_TRANSPOSED|| mode == Oblig2Precode.Mode.PARA_A_TRANSPOSED|| mode == Oblig2Precode.Mode.PARA_B_TRANSPOSED)
            a.parallelize();


        TimeTaker.TimingResult timingResult = TimeTaker.time(timesToRun, () -> a.multiply(b));
        Matrix result = (Matrix) timingResult.object;

        Oblig2Precode.saveResult(seed, mode, result.array);

        return result.array;
    }


    /**
     * @param results results to verify
     * @return true if all results were identical to SEQ_NOT_TRANSPOSED result
     */
    private static boolean verifyResults(HashMap<Integer, HashMap<Oblig2Precode.Mode, double[][]>> results) {
        // Compare all results to the sequential untransposed one, to ensure correct results
        boolean success = true;

        for (Integer key : results.keySet()) {
            if (DEBUG) System.out.println("\nTesting correct results for n = " + key);
            double[][] correctResult = results.get(key).get(Oblig2Precode.Mode.SEQ_NOT_TRANSPOSED);

            for (Oblig2Precode.Mode mode : Oblig2Precode.Mode.values()) {
                if (mode == Oblig2Precode.Mode.SEQ_NOT_TRANSPOSED)
                    continue;

                if (DEBUG) System.out.print("Comparing SEQ_NOT_TRANSPOSED with " + mode);

                // Here i am assuming that deepEquals handles the natural floating
                // point imprecision, since it seemed to work fine.
                if (Arrays.deepEquals(correctResult, results.get(key).get(mode))) {
                    if (DEBUG) System.out.println(" - OK!");
                } else {
                    if (DEBUG) System.out.println(" - FAILED!");
                    success = false;
                }
            }
        }
        return success;
    }

}

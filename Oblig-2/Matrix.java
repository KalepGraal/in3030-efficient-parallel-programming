import java.util.Arrays;

class Matrix {

    private static final boolean DEBUG_MATRIX = false;

    double[][] array;
    boolean isTransposed = false;
    private boolean parallel = false;

    Matrix(double[][] matrix) {
        this.array = matrix;
    }

    /**
     * Multiplies two matrices
     *
     * @param b matrix to multiply with
     * @return this matrix multiplied with b
     */
    Matrix multiply(Matrix b) {

        if (parallel)
            return new ParallelMatrix().multiply(this, b);

        int n = array.length;
        Matrix result = new Matrix(new double[n][n]);

        // Does making these final make the JIT better?
        final boolean aTransposed = isTransposed;
        final boolean bTransposed = b.isTransposed;

        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                for(int k=0;k<n;k++)
                    result.array[i][j] += (aTransposed ? array[k][i] : array[i][k])
                            * (bTransposed ? b.array[j][k] : b.array[k][j]);

        return result;
    }


    /**
     * Makes the matrix do its multiplication in parallel
     */
    void parallelize() {
        parallel = true;
    }

    /**
     * transposes the matrix
     */
    void transpose() {
        int n = array.length;

        if (DEBUG_MATRIX) {
            System.out.println("before transposing:");
            Arrays.stream(array).forEach(x -> System.out.println(Arrays.toString(x)));
            System.out.println();
        }

        for(int i=1;i<n;i++){
            for(int j=0;j<i;j++) {
                if (DEBUG_MATRIX) System.out.printf("switching %.1f & %.1f\n", array[i][j], array[j][i]);

                double tmp = array[i][j];
                array[i][j] = array[j][i];
                array[j][i] = tmp;
            }
        }

        if (DEBUG_MATRIX) {
            System.out.println("after transposing:");
            Arrays.stream(array).forEach(x -> System.out.println(Arrays.toString(x)));
            System.out.println();
        }

        isTransposed = true;
    }
}
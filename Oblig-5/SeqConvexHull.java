import java.util.ArrayList;
import java.util.Comparator;

class SeqConvexHull {
    int[] x, y;
    int n;
    private Oblig5Precode o5p;

    SeqConvexHull(int n, int[] x, int[] y) {
        this.n = n;
        this.x = x;
        this.y = y;
    }

    IntList find() {
        IntList hull = new IntList();
        int MAX_X = 0, MIN_X = 0;  // Points with max x and min x values

        // Find highest x and y values, and min/max x points
        int highestX = x[0], highestY = y[0];
        for (int i = 0; i < n; i++) {
            if (x[i] > highestX) {
                highestX = x[i];
                MAX_X = i;
            }
            if (x[i] < x[MIN_X]) {
                MIN_X = i;
            }
            if (y[i] > highestY) {
                highestY = y[i];
            }
        }

        o5p = new Oblig5Precode(n, x, y, hull, highestX, highestY, "Sequential");

        if (Main.RunWithGraphics) o5p.drawGraph();

        findConvexHull(hull, MIN_X, MAX_X);

        return hull;
    }


    void writeHullPoints() {
        o5p.writeHullPoints("SEQ");
    }


    void findConvexHull(IntList convexHull, int MIN_X, int MAX_X) {

        convexHull.add(MAX_X);

        // Create initial list of points to check
        IntList pointsToCheck = new IntList();
        for (int i = 0; i < n; i++) {
            pointsToCheck.add(i);
        }

        addNextPoint(new Line(MAX_X, MIN_X), pointsToCheck, convexHull);

        convexHull.add(MIN_X);

        addNextPoint(new Line(MIN_X, MAX_X), pointsToCheck, convexHull);
    }

    private void addNextPoint(Line line, IntList pointsToCheck, IntList hull) {
        IntList rightPoints = new IntList();

        // Store points that appear on this line
        // OK to use ArrayList here, since we expect this list to be fairly small,
        // and this enables the use of an inbuilt sort.
        ArrayList<Integer> pointsOnLine = new ArrayList<>();

        int furthest = -1; int dist_to_furthest = 0;
        for (int i = 0; i < pointsToCheck.size(); i++) {
            int point = pointsToCheck.get(i);
            int dist = line.distanceToPoint(point);

            if (dist < dist_to_furthest) {
                furthest = point;
                dist_to_furthest = dist;
            }
            if (furthest == -1 && dist == 0 && point != line.p1 && point != line.p2) {
                // As long as we haven't found a point not on the line, keep adding points on the line
                pointsOnLine.add(point);
            }
            if (dist <= 0) {  // right side
                rightPoints.add(point);
            }
        }
        // if we didn't find a new point, we should add points on the line, if any
        if (furthest == -1) {
            // Sort points on the line first, to ensure that they're added in correct order
            // Sorting by distance to p1
            pointsOnLine.sort(Comparator.comparingInt((i) -> Math.abs(x[i]+x[line.p1]) + Math.abs(y[i]+y[line.p1])));

            for (Integer pointOnLine : pointsOnLine) {
                hull.add(pointOnLine);
            }
        }

        if (furthest >= 0 && rightPoints.size() > 0) {
            Line nextLine = new Line(line.p1, furthest);
            addNextPoint(nextLine, rightPoints, hull);
            hull.add(furthest);
            nextLine = new Line(furthest, line.p2);
            addNextPoint(nextLine, rightPoints, hull);
        }
    }

    private class Line {
        int p1, p2;
        int a, b, c;

        Line(int p1, int p2) {
            this.p1 = p1;
            this.p2 = p2;

            // Creates a line between two points
            this.a = y[p1] - y[p2];
            this.b = x[p2] - x[p1];
            this.c = (y[p2] * x[p1]) - (y[p1] * x[p2]);
        }

        int distanceToPoint(int p) {
            // Simplified formula, since we only care about the relative distance
            return (((a * x[p]) + (b * y[p]) + c));
        }
    }

}

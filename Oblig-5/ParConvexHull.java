import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ParConvexHull {
    private int[] x, y;
    private int n;
    private Oblig5Precode o5p;
    private final static int overbook = 20;

    ParConvexHull(int n, int[] x, int[] y) {
        this.n = n;
        this.x = x;
        this.y = y;
    }

    IntList find() {

        TaskHandler th = new TaskHandler();

        MinMaxResult minMaxResult = th.getMinMaxValues();

        IntList hull = new IntList();
        th.findConvexHull(hull, minMaxResult.MIN_X, minMaxResult.MAX_X);

        o5p = new Oblig5Precode(n, x, y, hull, minMaxResult.highestX, minMaxResult.highestY, "Parallel");

        if (Main.RunWithGraphics) o5p.drawGraph();

        th.close();

        return hull;
    }


    void writeHullPoints() {
        o5p.writeHullPoints("PAR");
    }



    private class TaskHandler {
        // this class should handle workload distribution and execution of threads
        Worker[] workers;
        Task[] tasks;
        CyclicBarrier barrier;
        CountDownLatch taskCountDownLatch;
        ExecutorService pool;

        int taskCount = 0;
        int recDepthBeforeSeq;

        TaskHandler() {
            int numThreads = Runtime.getRuntime().availableProcessors();
            int numWorkers = numThreads;
            workers = new Worker[numWorkers];

            barrier = new CyclicBarrier(numWorkers + 1);
            pool = Executors.newFixedThreadPool(numThreads);

            // Find out deep we should do the recursion before going sequential
            float counter = numThreads * overbook;
            while (counter > 1.0) {
                recDepthBeforeSeq++;
                counter = counter / 2;
            }

            // Find out how many tasks we're creating, minimum 2 to avoid errors
            int numTasks = Math.max(2, (int) Math.pow(2, recDepthBeforeSeq + 1) - 2);
            tasks = new Task[numTasks];

            // Distribute points between workers
            Slice[] slices = createSlices(n, numWorkers);
            for (int i = 0; i < numWorkers; i++) {
                workers[i] = new Worker(slices[i]);
            }

            for (int i = 0; i < numTasks; i++) {
                tasks[i] = new Task();
            }
        }

        void setup() {
            taskCountDownLatch = new CountDownLatch(tasks.length);
        }

        void findConvexHull(IntList hull, int MIN_X, int MAX_X) {
            setup();

            // Create initial list of points to check
            IntList pointsToCheck = new IntList();
            for (int i = 0; i < n; i++) {
                pointsToCheck.add(i);
            }

            // Start our tasks, assumes we always have at least 2
            pool.execute(() -> tasks[0].addNextPoint(new Line(MAX_X, MIN_X), pointsToCheck, 1));

            pool.execute(() -> tasks[1].addNextPoint(new Line(MIN_X, MAX_X), pointsToCheck, 1));

            try {
                taskCountDownLatch.await();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            // collect points
            hull.add(MAX_X);
            hull.append(tasks[0].collectHull());
            hull.add(MIN_X);
            hull.append(tasks[1].collectHull());
        }


        MinMaxResult getMinMaxValues() {
            for (Worker worker : workers) {
                pool.execute(worker::runMinMax);
            }
            waitForBarrier(barrier);

            // Compare and collect results from workers
            MinMaxResult result = workers[0].minMaxResult;
            for (Worker worker : workers) {
                if (worker.minMaxResult.highestX > result.highestX) {
                    result.highestX = worker.minMaxResult.highestX;
                    result.MAX_X = worker.minMaxResult.MAX_X;
                }
                if (x[worker.minMaxResult.MIN_X] < x[result.MIN_X]) {
                    result.MIN_X = worker.minMaxResult.MIN_X;
                }
                if (worker.minMaxResult.highestY > result.highestY) {
                    result.highestY = worker.minMaxResult.highestY;
                }
            }

            return result;
        }

        void close() {
            pool.shutdown();
        }

        class Task {
            int taskID;
            IntList MyHull = new IntList();

            Task() {
                taskID = taskCount++;
            }

            IntList collectHull() {
                if (2*(taskID+1) >= tasks.length)
                    return MyHull;

                IntList accHull = tasks[2*(taskID+1)].collectHull();
                accHull.append(MyHull);
                accHull.append(tasks[2*(taskID+1)+1].collectHull());
                return accHull;
            }

            private void addNextPoint(Line line, IntList pointsToCheck, int depth) {
                // Finds the next points in the hull, in parallel

                // We continue adding tasks to our thread-pool until a given recursive depth is reached
                if (depth < recDepthBeforeSeq) {
                    IntList rightPoints = new IntList();
                    ArrayList<Integer> pointsOnLine = new ArrayList<>();

                    // find the furthest point and points to the right of or on the line
                    int furthest = findFurthestAndRightPoints(line, pointsToCheck,
                                                              rightPoints, pointsOnLine);

                    if (furthest == -1) {
                        // Sort so that points on line are added in correct order
                        pointsOnLine.sort(Comparator.comparingInt((i) -> Math.abs(x[i]+x[line.p1]) + Math.abs(y[i]+y[line.p1])));

                        for (Integer pointOnLine : pointsOnLine) {
                            MyHull.add(pointOnLine);
                        }
                    }

                    if (furthest >= 0 && rightPoints.size() > 0) {
                        pool.execute(() -> tasks[2*(taskID+1)].addNextPoint(new Line(line.p1, furthest), rightPoints, depth+1));

                        MyHull.add(furthest);
                        pool.execute(() -> tasks[2*(taskID+1)+1].addNextPoint(new Line(furthest, line.p2), rightPoints, depth+1));
                    } else {
                        // No more tasks will be started from this one, since no points exists
                        // mark child tasks as done
                        recEndSubTasks(taskID);
                    }

                } else {
                    // We reached the depth where the rest will run sequentially
                    // will therefore no longer start new tasks, mark child tasks as done
                    recEndSubTasks(taskID);

                    // Find the next points
                    SEQaddNextPoint(line, pointsToCheck, MyHull);
                }

                endTask();
            }

            void endTask() {
                // Mark task as done
                taskCountDownLatch.countDown();
            }

            void recEndSubTasks(int id) {
                // End tasks that would've been started tasks[id]
                if (2*(id+1) < tasks.length) {
                    tasks[2*(id+1)].endTask();
                    recEndSubTasks(2*(id+1));
                }
                if (2*(id+1)+1 < tasks.length) {
                    tasks[2*(id+1)+1].endTask();
                    recEndSubTasks(2*(id+1)+1);
                }
            }


            int findFurthestAndRightPoints(Line line, IntList pointsToCheck, IntList rightPoints, ArrayList<Integer> pointsOnLine) {
                // Finds the point furthest from the line
                // Also finds all points to the right of or on the line.

                int furthest = -1; int dist_to_furthest = 0;
                for (int i = 0; i < pointsToCheck.size(); i++) {
                    int point = pointsToCheck.get(i);
                    int dist = line.distanceToPoint(point);

                    if (dist < dist_to_furthest) {
                        furthest = point;
                        dist_to_furthest = dist;
                    }
                    if (furthest == -1 && dist == 0 && point != line.p1 && point != line.p2) {
                        // As long as we haven't found a point not on the line, keep adding points on the line
                        pointsOnLine.add(point);
                    }
                    if (dist <= 0) {  // right side
                        rightPoints.add(point);
                    }
                }

                return furthest;
            }

            private void SEQaddNextPoint(Line line, IntList pointsToCheck, IntList hull) {
                // Sequentially finds the next points in the hull

                IntList rightPoints = new IntList();

                // Store points that appear on this line
                // OK to use ArrayList here, since we expect this list to be fairly small,
                // and this enables the use of an inbuilt sort.
                ArrayList<Integer> pointsOnLine = new ArrayList<>();


                int furthest = findFurthestAndRightPoints(line, pointsToCheck,
                                                          rightPoints, pointsOnLine);

                // if we didn't find a new point, we should add points on the line, if any
                if (furthest == -1) {
                    // Sort points on the line first, to ensure that they're added in correct order
                    // Sorting by distance to p1
                    pointsOnLine.sort(Comparator.comparingInt((i) -> Math.abs(x[i]+x[line.p1]) + Math.abs(y[i]+y[line.p1])));

                    for (Integer pointOnLine : pointsOnLine) {
                        hull.add(pointOnLine);
                    }
                }

                if (furthest >= 0) {
                    if (rightPoints.size() > 0) {
                        Line nextLine = new Line(line.p1, furthest);
                        SEQaddNextPoint(nextLine, rightPoints, hull);
                        hull.add(furthest);
                        nextLine = new Line(furthest, line.p2);
                        SEQaddNextPoint(nextLine, rightPoints, hull);
                    }
                }
            }
        }



        class Worker {
            // Worker to find extreme values for our points

            Slice slice;
            MinMaxResult minMaxResult = new MinMaxResult();

            Worker(Slice slice) {
                this.slice = slice;
            }

            void runMinMax() {
                // Set initial values
                minMaxResult.highestX = x[slice.from];
                minMaxResult.highestY = y[slice.from];
                minMaxResult.MIN_X = slice.from;
                minMaxResult.MAX_X = slice.from;

                // Go through entire slice, and find results
                for (int i = slice.from; i < slice.to; i++) {
                    if (x[i] > minMaxResult.highestX) {
                        minMaxResult.highestX = x[i];
                        minMaxResult.MAX_X = i;
                    }
                    if (x[i] < x[minMaxResult.MIN_X]) {
                        minMaxResult.MIN_X = i;
                    }
                    if (y[i] > minMaxResult.highestY) {
                        minMaxResult.highestY = y[i];
                    }
                }
                waitForBarrier(barrier);
            }

        }

        class Slice {
            // Represents a index range a worker works on
            final int from;
            final int to;

            Slice(int from, int to) {
                this.from = from;
                this.to = to;
            }
        }

        private Slice[] createSlices(int arrLength, int numSlices) {
            // Helper function to divide an array between workers

            Slice[] slices = new Slice[numSlices];
            int slice = arrLength / numSlices;
            int rest = arrLength % numSlices;
            int offset = 0;
            for (int i = 0; i < numSlices; i++) {
                int low = i * slice + offset;
                int high = low + slice;

                if (rest > 0) {
                    high++; offset++; rest--;
                }
                slices[i] = new Slice(low, high);
            }
            return slices;
        }

        private void waitForBarrier(CyclicBarrier barrier) {
            try {
                barrier.await();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


    private class MinMaxResult {
        int highestX, highestY;     // highest values for x and y
        int MIN_X = 0, MAX_X = 0;   // points with highest and lowest x value
    }

    private class Line {
        int p1, p2;
        int a, b, c;

        Line(int p1, int p2) {
            this.p1 = p1;
            this.p2 = p2;

            // Creates a line between two points
            this.a = y[p1] - y[p2];
            this.b = x[p2] - x[p1];
            this.c = (y[p2] * x[p1]) - (y[p1] * x[p2]);
        }

        int distanceToPoint(int p) {
            // Simplified formula, since we only care about the relative distance
            return (((a * x[p]) + (b * y[p]) + c));
        }
    }

}

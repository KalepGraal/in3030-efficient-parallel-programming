import java.util.HashMap;

public class Main {

    private static HashMap<Integer, TimeTaker.TimingResult<IntList>> seqResults = new HashMap<>();
    private static HashMap<Integer, TimeTaker.TimingResult<IntList>> parResults = new HashMap<>();

    static boolean RunWithGraphics = false;
    private static final boolean runSeq = true;
    private static final boolean runPar = true;

    public static void main(String[] args){

        int seed = 123;
        int from = 100, to = 100_000_000;

        if (args.length > 0) {
            seed = Integer.parseInt(args[0]);
            if (args.length > 1) {
                int n = Integer.parseInt(args[1]);
                from = n; to = n;
                RunWithGraphics = true;
            }
        }

        int timesToTime = RunWithGraphics ? 1 : 7;

        for (int i = from; i <= to; i*=10) {
            // Create initial points
            int[] x = new int[i];
            int[] y = new int[i];
            NPunkter17 points = new NPunkter17(i, seed);
            points.fyllArrayer(x, y);

            System.out.printf("############ n = %d ############\n", i);
            if (runSeq) {
                SeqConvexHull seqConvexHull = new SeqConvexHull(i, x, y);
                seqResults.put(i, TimeTaker.time(timesToTime, seqConvexHull::find));
                seqConvexHull.writeHullPoints();
            }

            if (runPar) {
                ParConvexHull parConvexHull = new ParConvexHull(i, x, y);
                parResults.put(i, TimeTaker.time(timesToTime, parConvexHull::find));
                parConvexHull.writeHullPoints();
            }
        }

        System.out.format("\n%-12s%-10s%-10s%-10s%-10s\n", "n", "Seq", "Para", "Speedup", "Times ran");
        for (int i = from; i <= to; i*=10) {
            String[] lastResult = new String[] {
                    Integer.toString(i),
                    (runSeq ? Long.toString(seqResults.get(i).timeInMs()) : "-") + " ms",
                    (runPar ? Long.toString(parResults.get(i).timeInMs()) : "-") + " ms",
                    runSeq && runPar ? String.format("%.2f", (double)seqResults.get(i).time/parResults.get(i).time) : "-",
                    Integer.toString(timesToTime)};

            System.out.format("%-12s%-10s%-10s%-10s%-10s\n", (Object[]) lastResult);
        }

    }
}